# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Load ~/.profile, if it exists
[ -r "${HOME}/.profile" ] && . "${HOME}/.profile"

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

#
# Bash Prompt (displays '^ssh' if on a ssh connection)
#
# Inspired by "Extravagent Zsh Prompt" by Steve Losh
# Based off of @gf3's "Sexy Bash Prompt"
#

# Define some colors
if tput setaf 1 &> /dev/null; then
    tput sgr0
    if [ $(tput colors) -ge 256 ] 2>/dev/null; then
        red=$(tput setaf 9)
        yellow=$(tput setaf 226)
        green=$(tput setaf 118)
        blue=$(tput setaf 39)
        purple=$(tput setaf 141)
        cyan=$(tput setaf 117)
        white=$(tput setaf 255)
        grey=$(tput setaf 233)
        orange=$(tput setaf 214)
    else
        red=$(tput setaf 1)
        yellow=$(tput setaf 3)
        green=$(tput setaf 2)
        blue=$(tput setaf 4)
        purple=$(tput setaf 5)
        cyan=$(tput setaf 6)
        white=$(tput setaf 7)
        grey=$(tput setaf 8)
        orange=${yellow}
    fi
    bold=$(tput bold)
    reset=$(tput sgr0)
else
    red="\033[1;31m"
    yellow="\033[1;33m"
    green="\033[1;32m"
    blue="\033[1;34m"
    purple="\033[1;35m"
    cyan="\033[1;36m"
    white="\033[1;37m"
    grey="\033[1;30m"
    orange=${yellow}
    bold=""
    reset="\033[00m"
fi

return_code_promptchar () {
    ret=$?

    # Make ${prompt_char} green if return code is zero, red otherwise
    if [ ${ret} -eq 0 ]; then
        printf "\001${green}\002"
    else
        printf "\001${red}\002"
    fi

    # Determine the prompt character
    if in_vcs_repo 'git'; then
        prompt_char='⚡'
    elif in_vcs_repo 'hg'; then
        prompt_char='☿'
    elif in_vcs_repo 'svn'; then
        prompt_char='±'
    elif [ $(id -ru) -eq 0 ]; then
        prompt_char='#'
    else
        prompt_char='$'
    fi

    printf " ${prompt_char} "
}

# Determine if we are in a vcs repository (git, hg, etc.)
in_vcs_repo () {
    [ -n "$1" ] && vcs=".$1" || return 1
    d=.

    while [ ! -d ${d}/${vcs} ] && [ "`readlink -e ${d}`" != "/" ]
    do
        d=${d}/..
    done

    [ -d ${d}/${vcs} ]
    return $?
}

# Grab name of current branch, then analyze working directory
parse_git () {
    local dirty=''
    local git_status="$(git status 2>/dev/null)"
    local branch="$(git status 2>/dev/null | \
        grep 'On branch' | sed 's/# On branch \(.*\)/\1/')"

    echo $git_status | grep 'Untracked files' &>/dev/null && \
        dirty='?'
    echo $git_status | grep 'Changes not staged for commit' &>/dev/null && \
        dirty='*'
    echo $git_status | grep 'Changes to be committed' &>/dev/null && \
        dirty='!'

    printf "${white}on ${purple}${branch}${red}${dirty}"
}

parse_vcs () {
    ret=$?
    if in_vcs_repo "git"; then
        parse_git
    elif in_vcs_repo "hg"; then
        hg prompt "${white}on ${purple}{branch}{status} ${white}at ${red}{tags|, }"
    fi
    return $ret
}

PS1=""


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
