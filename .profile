## ~/.profile

alias df='df -h'
alias du='du -ch'
alias diff='colordiff'
alias grep='grep --color=auto'
alias gzip='pigz'
alias mkdir='mkdir -p'
alias more='less'
alias open='xdg-open'
alias pacman='pacman-color'
alias ping='ping -c 5'
alias ge='gedit'
alias home='cd ~'
alias desk='cd ~/Desktop/'
alias dl='cd ~/Downloads/'

alias ls='ls -hF --color=auto'
alias lr='ls -R'
alias ll='ls -l'
alias la='ls -a'

if [ $(id -ru) -ne 0 ]; then
    alias sudo='sudo '
    alias scat='sudo cat'
    alias sge='sudo gedit'
fi

alias chown='chown --preserve-root'
alias chgrp='chgrp --preserve-root'
alias chmod='chmod --preserve-root'

# Easier navigation
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# Commonly used commands
alias c='clear'
alias g='git'
alias gad='git add .'
alias gcm='git commit -m'
alias l='ls'
alias adb='/home/akonwi/Android/platform-tools/adb'

EDITOR='gedit'
LESS='-R'
PATH="${PATH}:${HOME}/bin/:${HOME}/.gem/ruby/1.9.3/bin/"

export EDITOR
export LESS
export PACMAN
export PATH

# Execute SML runtime
ml () {
    if [ -x "`which sml 2>/dev/null`" ]; then
        sml
    elif [ -x "`which poly 2>/dev/null`" ]; then
        poly
    else
        echo 'No SML runtime installed!' >&2
        return 1
    fi
}

# 256-color Terminal
case "${TERM}" in
    *-256color)
        alias ssh='TERM=${TERM%-256color} ssh'
        ;;
    *)
        term="${TERM}-256color"
        terminfo="`echo ${term} | cut -c 1`/${term}"

        [ -f /usr/share/terminfo/${terminfo} ] && TERM=${term}
        [ -f ${HOME}/${terminfo} ] && TERM=${term}
        [ "${TERM}" = "${term}" ] && export TERM
        ;;
esac
